# Change Log

## [v0.2.2](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-firmware/-/releases/v0.2.2) 2021/10/01

* MAC address is now shown at startup and via HTTP

## [v0.2.1](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-firmware/-/releases/v0.2.1) 2021/06/06

* BUGFIX: Sensor would keep on calibrating after successful calibration.

## [v0.2.0](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-firmware/-/releases/v0.2.0) 2021/06/06

* BUGFIX: Calibration was not applied correctly (Thanks Michael Käppler for bug finding & fixing!)
* MQTT works again for ESP32 (board needs to be updated in Arduino IDE/PlaftormIO)

## [v0.1.0](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-firmware/-/releases/v0.1.0) 2021/05/19

* More stable calibration, with 10s timestep
* Allow commands from Serial/MQTT/Webserver
* Many bugfixes

## [v0.0.9](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-firmware/-/releases/v0.0.9) 2020/12/15

* First CO2 Ampel release.
* Basic functionality
