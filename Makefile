all:
	pio -f -c vim run

upload:
	pio -f -c vim run --target upload -e $(board)

clean:
	pio -f -c vim run --target clean

program:
	pio -f -c vim run --target program

uploadfs:
	pio -f -c vim run --target uploadfs

update:
	pio -f -c vim update

monitor:
	pio device monitor --filter colorize
