# CO<sub>2</sub> Ampel
[![Build Status](https://drone.makerlab-murnau.de/api/badges/CO2-ampel/software/status.svg)](https://drone.makerlab-murnau.de/CO2-ampel/software)

*CO<sub>2</sub> Ampel* is an open-source project, written in C++ for [ESP8266](https://en.wikipedia.org/wiki/ESP8266) or [ESP32](https://en.wikipedia.org/wiki/ESP32).

It measures the current CO<sub>2</sub> concentration (in ppm), and displays it on an LED ring.

The room should be ventilated as soon as one LED turns red.

## Features

The *CO<sub>2</sub> Ampel* can:

* Display CO2 concentration on LED ring.
* Allow calibration.
* Get current time over [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol)
* Send data over [MQTT](https://en.wikipedia.org/wiki/MQTT).
* Send data over [LoRaWAN](https://en.wikipedia.org/wiki/LoRa#LoRaWAN).
* Display measurements and configuration on a small website.
* Log data to a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) file, directly on the ESP flash memory.
* Accept many interactive commands.

## Hardware Requirements

* [ESP8266](https://en.wikipedia.org/wiki/ESP8266) or [ESP32](https://en.wikipedia.org/wiki/ESP32) microcontroller (this project has been tested with *ESP8266 ESP-12 WIFI* and *TTGO ESP32 SX1276 LoRa*)
* [Sensirion SCD30](https://www.sensirion.com/en/environmental-sensors/carbon-dioxide-sensors/carbon-dioxide-sensors-co2/) "Sensor Module for HVAC and Indoor Air Quality Applications"
* [NeoPixel Ring - 12](https://www.adafruit.com/product/1643), or [NeoPixel Ring - 16](https://www.adafruit.com/product/1463) (experimental)

See the [documentation](https://transfer.hft-stuttgart.de/gitlab/co2ampel/ampel-documentation) for more info.

## Software Requirements

* [PlatformIO](https://platformio.org/)
or
* [Arduino IDE](https://www.arduino.cc/en/software)

## Installation

* If `config.h` does not exist, copy it from `config.public.h`
* Modify `config.h`, e.g. for measurement time-steps, WiFi access, MQTT, NTP and web-server.

### PlatformIO

PlatformIO can be run from [VSCODE](https://platformio.org/install/ide?install=vscode), [Eclipse CDT](https://www.eclipse.org/cdt/) or console:

```bash
make upload board=esp8266 && make monitor # For ESP8266
```
```bash
make upload board=esp32 && make monitor # For ESP32
```

### Arduino IDE

* All the libraries are included in this repository. No need to install anything via *Library Manager*.

* Add your board to the [board manager](https://github.com/esp8266/Arduino#installing-with-boards-manager). Either ESP8266:

        http://arduino.esp8266.com/stable/package_esp8266com_index.json

  or ESP32:

        https://dl.espressif.com/dl/package_esp32_index.json

* Choose the correct board in *Tools > Board > ...*
* Choose the correct *Flash size* (e.g. "Flash Size : 4MB (1MB FS, OTA:~1019kB)" for *ESP8266 ESP-12 WIFI*)

* *Verify*
* *Upload*
* *Tools > Serial Monitor*

## Available commands

In Arduino IDE *Serial Monitor* or PlatformIO *Monitor*, type `help` + <kbd>Enter</kbd> in order to list the available commands:

* `auto_calibrate 0/1` (Disables/enables autocalibration).
* `calibrate` (Starts calibration process).
* `calibrate 600` (Starts calibration process, to given ppm).
* `calibrate! 600` (Calibrates right now, to given ppm).
* `co2 1500` (Sets co2 level, for debugging purposes).
* `color 0xFF0015` (Shows color, specified as RGB, for debugging).
* `csv 60` (Sets CSV writing interval, in s).
* `format_filesystem` (Deletes the whole filesystem).
* `free` (Displays available heap space).
* `local_ip` (Displays local IP and current SSID).
* `lora 300` (Sets LoRaWAN sending interval, in s).
* `mqtt 60` (Sets MQTT sending interval, in s).
* `night_mode` (Toggles night mode on/off).
* `reset` (Restarts the ESP).
* `reset_scd` (Resets SCD30).
* `send_local_ip` (Sends local IP and SSID via MQTT. Can be useful to find sensor).
* `set_time 1618829570` (Sets time to the given UNIX time).
* `show_csv` (Displays the complete CSV file on Serial).
* `timer 30` (Sets measurement interval, in s).
* `wifi_scan` (Scans available WiFi networks).

The commands can be sent via the Serial interface, from the webpage or via MQTT.

## Authors

 * Eric Duminil (HfT Stuttgart)
 * Robert Otto (HfT Stuttgart)
 * Myriam Guedey (HfT Stuttgart)
 * Tobias Gabriel Erhart (HfT Stuttgart)
 * Jonas Stave (HfT Stuttgart)
 * Michael Käppler

## Contributing

* Merge requests are welcome, and should be based on the `develop` branch.
* The `develop` branch gets merged into the `master` once it has been sufficiently tested.
* For major changes, please open an issue first to discuss what you would like to change.

## License

Copyright © 2021, [HfT Stuttgart](https://www.hft-stuttgart.de/)
[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
