#ifndef AMPEL_H_INCLUDED
#define AMPEL_H_INCLUDED
/*****************************************************************
 * Libraries                                                     *
 *****************************************************************/
#include "config.h"
#ifndef MEASUREMENT_TIMESTEP
#  error Missing config.h file. Please copy config.public.h to config.h.
#endif

#ifdef AMPEL_CSV
#  include "csv_writer.h"
#endif

#ifdef AMPEL_WIFI
#  include "wifi_util.h"
#  ifdef AMPEL_MQTT
#    include "mqtt.h"
#  endif
#  ifdef AMPEL_HTTP
#    include "web_server.h"
#  endif
#  if defined(ESP8266)
     //allows sensor to be seen as SENSOR_ID.local, from the local network. For example : espd03cc5.local
#    include <ESP8266mDNS.h>
#  elif defined(ESP32)
#    include <ESPmDNS.h>
#  endif
#endif

#ifdef AMPEL_LORAWAN
#  include "lorawan.h"
#endif

#include "util.h"
#include "sensor_console.h"
#include "co2_sensor.h"
#include "led_effects.h"

#endif
