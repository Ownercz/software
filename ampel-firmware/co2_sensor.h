#ifndef CO2_SENSOR_H_
#define CO2_SENSOR_H_

// The SCD30 from Sensirion is a high quality Nondispersive Infrared (NDIR) based CO₂ sensor capable of detecting 400 to 10000ppm with an accuracy of ±(30ppm+3%).
// https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include "src/lib/SparkFun_SCD30_Arduino_Library/src/SparkFun_SCD30_Arduino_Library.h"  // From: http://librarymanager/All#SparkFun_SCD30
#include "config.h"
#include "led_effects.h"
#include "util.h"
#include "sensor_console.h"
#include <Wire.h>

namespace config {
  extern uint16_t measurement_timestep; // [s] Value between 2 and 1800 (range for SCD30 sensor)
  extern bool auto_calibrate_sensor; // [true / false]
  extern uint16_t co2_calibration_level; // [ppm]
  extern const float temperature_offset; // [K] Sign isn't relevant.
}

namespace sensor {
  extern SCD30 scd30;
  extern uint16_t co2;
  extern float temperature;
  extern float humidity;
  extern char timestamp[];

  void initialize();
  bool processData();
  void startCalibrationProcess();

  void setCO2forDebugging(int32_t fakeCo2);
  void setTimer(int32_t timestep);
  void calibrateSensorToSpecificPPM(int32_t calibrationLevel);
  void calibrateSensorRightNow(int32_t calibrationLevel);
  void setAutoCalibration(int32_t autoCalibration);
  void resetSCD();
}
#endif
