#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define buttonPin 0
// This file is a config template, and can be copied to config.h. Please don't save any important password in this template.

// This is the level at which the magenta LED color signals need for calibration
// in some case it is irritating when this happens so you can lower this level if need - but use with care. Calibration shuld be done time to time
#define CALIBRATE_LEVEL 250

/**
 * SERVICES
 */

// Comment or remove those lines if you want to disable the corresponding services
//#  define AMPEL_WIFI    // Should ESP connect to WiFi? It allows the Ampel to get time from an NTP server.
//#  define AMPEL_HTTP    // Should HTTP web server be started? (AMPEL_WIFI should be enabled too)
//#  define AMPEL_MQTT    // Should data be sent over MQTT? (AMPEL_WIFI should be enabled too)
//#  define AMPEL_CSV     // Should data be logged as CSV, on the ESP flash memory?
// #  define AMPEL_LORAWAN // Should data be sent over LoRaWAN? (Requires ESP32 + LoRa modem, and "MCCI LoRaWAN LMIC library")

/**
 * WIFI
 */

#  define WIFI_SSID     "MY_SSID"
#  define WIFI_PASSWORD "P4SSW0RD"
#  define WIFI_TIMEOUT  30 // [s]

/**
 * Sensor
 */

// How often should measurement be performed, and displayed?
//WARNING: On some sensors, measurements become very unreliable when timestep is set to 2s.
//NOTE: 10s or longer should be fine in order to get reliable results.
//NOTE: SCD30 timer does not seem to be very precise. Time variations may occur.
#  define MEASUREMENT_TIMESTEP 5 // [s] Value between 2 and 1800 (range for SCD30 sensor)

// How often should measurements be appended to CSV ?
// Probably a good idea to use a multiple of MEASUREMENT_TIMESTEP, so that averages can be calculated
// Set to 0 if you want to send values after each measurement
// WARNING: Writing too often might damage the ESP memory
#  define CSV_INTERVAL 300 // [s]

// Residual heat from CO2 sensor seems to be high enough to change the temperature reading. How much should it be offset?
// NOTE: Sign isn't relevant. The returned temperature will always be shifted down.
#  define TEMPERATURE_OFFSET -3 // [K]

// Altitude above sea level
// Used for CO2 calibration
// here: Stuttgart, Schellingstr. 24. (Source: Google Earth)
#  define ALTITUDE_ABOVE_SEA_LEVEL 600 // [m]

// The reference CO2 concentration has to be within the range 400 ppm ≤ cref(CO2) ≤ 2000 ppm.
// Used for CO2 calibration
// here : measured concentration in Stuttgart
#  define ATMOSPHERIC_CO2_CONCENTRATION 425 // [ppm]

// Should the sensor try to calibrate itself?
// Sensirion recommends 7 days of continuous readings with at least 1 hour a day of 'fresh air' for self-calibration to complete.
#  define AUTO_CALIBRATE_SENSOR true // [true / false]

/**
 * LEDs
 */

// LED brightness, which can vary between min and max brightness ("LED breathing")
// MAX_BRIGHTNESS must be defined, and should be between 0 and 255.
#  define MAX_BRIGHTNESS 128
// MIN_BRIGHTNESS, if defined, should be between 0 and MAX_BRIGHTNESS - 1
// If MIN_BRIGHTNESS is not set, or if it is set to MAX_BRIGHTNESS, breathing is disabled.
#  define MIN_BRIGHTNESS 60
// How many LEDs in the ring? 12 and 16 are currently supported. If undefined, 12 is used as default.
#  define LED_COUNT 12

/**
 * WEB SERVER
 * available at http://local_ip, with user HTTP_USER and password HTTP_PASSWORD
 */

// Define empty strings in order to disable authentication, or remove the constants altogether.
#  define HTTP_USER "co2ampel"
#  define HTTP_PASSWORD "my_password"

/**
 * MQTT
 */

/*
 * If AMPEL_MQTT is enabled, co2ampel will publish data every MQTT_SENDING_INTERVAL seconds.
 * An MQTT subscriber can then get the data from the corresponding broker, either encrypted or unencrypted:
 *
 *  ❯ mosquitto_sub -h 'test.mosquitto.org' -p 8883 -t 'CO2sensors/#' --cafile mosquitto.org.crt -v
 *  CO2sensors/ESPd03cc5 {"time":"2020-12-13 13:14:37+01", "co2":571, "temp":18.9, "rh":50.9}
 *  CO2sensors/ESPd03cc5 {"time":"2020-12-13 13:14:48+01", "co2":573, "temp":18.9, "rh":50.2}
 *  ...
 *
 *  ❯ mosquitto_sub -h 'test.mosquitto.org' -t 'CO2sensors/#' -v
 *  CO2sensors/ESPd03cc5 {"time":"2020-12-13 13:15:09+01", "co2":568, "temp":18.9, "rh":50.1}
 *  CO2sensors/ESPd03cc5 {"time":"2020-12-13 13:15:20+01", "co2":572, "temp":18.9, "rh":50.3}
 *  ...
 */

/*
 * Allow sensor to be configured over MQTT? Very useful for debugging. For example:
 *   mosquitto_pub -h 'test.mosquitto.org' -t 'CO2sensors/ESPe08dc9/control' -m 'timer 30'
 *   mosquitto_pub -h 'test.mosquitto.org' -t 'CO2sensors/ESPe08dc9/control' -m 'calibrate'
 *   mosquitto_pub -h 'test.mosquitto.org' -t 'CO2sensors/ESPe08dc9/control' -m 'reset'
 */
#  define ALLOW_MQTT_COMMANDS false

// How often should measurements be sent to MQTT server?
// Probably a good idea to use a multiple of MEASUREMENT_TIMESTEP, so that averages can be calculated
// Set to 0 if you want to send values after each measurement
#  define MQTT_SENDING_INTERVAL MEASUREMENT_TIMESTEP * 5 // [s]
//#  define MQTT_SENDING_INTERVAL 60 // [s]
#  define MQTT_SERVER "test.mosquitto.org"  // MQTT server URL or IP address
#  define MQTT_PORT 8883
#  define MQTT_USER ""
#  define MQTT_PASSWORD ""

/**
 * LoRaWAN
 */

// 1) Requires "MCCI LoRaWAN LMIC library", which will be automatically used with PlatformIO but should be added in "Arduino IDE".
// 2) Region and transceiver type should be specified in:
//     * Arduino/libraries/MCCI_LoRaWAN_LMIC_library/project_config/lmic_project_config.h for Arduino IDE
//     * platformio.ini for PlatformIO
//    See https://github.com/mcci-catena/arduino-lmic#configuration for more information
// 3) It has been tested with "TTGO ESP32 SX1276 LoRa 868" and will only work with an ESP32 + LoRa modem
// 4) In order to use LoRaWAN, a gateway should be close to the co2ampel, and an account, an application and a device should be registered,
//      e.g. on https://www.thethingsnetwork.org/docs/applications/
// 5) The corresponding keys should be defined in LORAWAN_DEVICE_EUI, LORAWAN_APPLICATION_EUI and LORAWAN_APPLICATION_KEY
// How often should measurements be sent over LoRaWAN?
#  define LORAWAN_SENDING_INTERVAL 300 // [s] This value should not be too low. See https://www.thethingsnetwork.org/docs/lorawan/duty-cycle.html#maximum-duty-cycle

// WARNING: If AMPEL_LORAWAN is enabled, you need to modify the 3 following constants!
// This EUI must be in little-endian format, so least-significant-byte first.
// When copying an EUI from ttnctl output, this means to reverse the bytes.
#  define LORAWAN_DEVICE_EUI      {0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11}
// This should also be in little endian format, see above.
// For TheThingsNetwork issued EUIs the last bytes should be 0xD5, 0xB3, 0x70.
#  define LORAWAN_APPLICATION_EUI {0x00, 0x00, 0x00, 0x00, 0x00, 0xD5, 0xB3, 0x70}
// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
#  define LORAWAN_APPLICATION_KEY {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

/**
 * NTP
 */

#  define NTP_SERVER "pool.ntp.org"
#  define UTC_OFFSET_IN_SECONDS 7200 // [s] 3600 for UTC+1, 7200 for UTC+1 and daylight saving time

/**
 * Others
 */
#  define BAUDS 115200 // Transmission rate

#endif
