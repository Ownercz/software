#ifndef CSV_WRITER_H_
#define CSV_WRITER_H_

#if defined(ESP8266)
#  include <LittleFS.h>
#  define FS_LIB LittleFS
#elif defined(ESP32)
#  include <SPIFFS.h>
#  define FS_LIB SPIFFS
#else
#  error Board should be either ESP8266 or ESP832
#endif

#include "config.h"
#include "util.h"
#include "led_effects.h"
#include "sensor_console.h"

namespace config {
  extern uint16_t csv_interval; // [s]
}
namespace csv_writer {
  extern char last_successful_write[];
  void initialize(const char *sensorId);
  void logIfTimeHasCome(const char *timestamp, const int16_t &co2, const float &temperature, const float &humidity);
  int getAvailableSpace();
  extern char filename[];

  void setCSVinterval(int32_t csv_interval);
  void showCSVContent();
  void formatFilesystem();
}

#endif
