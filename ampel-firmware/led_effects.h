#ifndef LED_EFFECTS_H_INCLUDED
#define LED_EFFECTS_H_INCLUDED
#include <Arduino.h>
#include "config.h"
#include "sensor_console.h"

// Adafruit NeoPixel (Arduino library for controlling single-wire-based LED pixels and strip)
// https://github.com/adafruit/Adafruit_NeoPixel
// Documentation : http://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html
#include "src/lib/Adafruit_NeoPixel/Adafruit_NeoPixel.h"

namespace color {
  const uint32_t red = 0xFF0000;
  const uint32_t green = 0x00FF00;
  const uint32_t blue = 0x0000FF;
  const uint32_t black = 0x000000;
  const uint32_t magenta = 0xFF00FF;
}

namespace led_effects {
  void setupOnBoardLED();
  void onBoardLEDOff();
  void onBoardLEDOn();
  void toggleNightMode();
  void LEDsOff();

  void setupRing();
  void redAlert();
  bool countdownToZero();
  void showWaitingLED(uint32_t color);
  void showKITTWheel(uint32_t color, uint16_t duration_s = 2);
  void showRainbowWheel(uint16_t duration_ms = 1000);
  void displayCO2color(uint16_t co2);
}
#endif
