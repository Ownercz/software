#ifndef AMPEL_LORAWAN_H_
#define AMPEL_LORAWAN_H_

#include "config.h"

#if defined(AMPEL_LORAWAN) && defined(ESP32)
#include <Arduino.h>
// Requires "MCCI LoRaWAN LMIC library", which will be automatically used with PlatformIO but should be added in "Arduino IDE".
// Tested successfully with v3.2.0 and connected to a thethingsnetwork.org app.
#include <lmic.h>
#include <hal/hal.h>
#include <arduino_lmic_hal_boards.h>
#include <SPI.h>

#include "led_effects.h"
#include "sensor_console.h"
#include "util.h"

namespace config {
  extern uint16_t lorawan_sending_interval; // [s]
}

#if defined(CFG_eu868)
#  define LMIC_FREQUENCY_PLAN "Europe 868"
#elif defined(CFG_us915)
#  define LMIC_FREQUENCY_PLAN "US 915"
#elif defined(CFG_au915)
#  define LMIC_FREQUENCY_PLAN "Australia 915"
#elif defined(CFG_as923)
#  define LMIC_FREQUENCY_PLAN "Asia 923"
#elif defined(CFG_kr920)
#  define LMIC_FREQUENCY_PLAN "Korea 920"
#elif defined(CFG_in866)
#  define LMIC_FREQUENCY_PLAN "India 866"
#else
#  error "Region should be specified"
#endif

namespace lorawan {
  extern bool waiting_for_confirmation;
  extern bool connected;
  extern char last_transmission[];
  void initialize();
  void process();
  void preparePayloadIfTimeHasCome(const int16_t &co2, const float &temp, const float &hum);

  void setLoRaInterval(int32_t sending_interval);
}

#endif
#endif
