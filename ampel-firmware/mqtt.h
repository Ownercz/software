#ifndef MQTT_H_INCLUDED
#define MQTT_H_INCLUDED

#include <Arduino.h>
#include "config.h"
#include "led_effects.h"
#include "sensor_console.h"
#include "src/lib/PubSubClient/src/PubSubClient.h"
#include "wifi_util.h"

namespace config {
  extern uint16_t mqtt_sending_interval; // [s]
}

namespace mqtt {
  extern char last_successful_publish[];
  extern bool connected;
  void initialize(const char *sensorId);
  void keepConnection();
  void publishIfTimeHasCome(const char *timestamp, const int16_t &co2, const float &temp, const float &hum);

  void setMQTTinterval(int32_t sending_interval);
  void sendInfoAboutLocalNetwork();
}
#endif
