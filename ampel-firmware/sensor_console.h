#ifndef SENSOR_CONSOLE_H_INCLUDED
#define SENSOR_CONSOLE_H_INCLUDED
#include <Arduino.h>

/** Other scripts can use this namespace, in order to define commands, via callbacks.
 * Those callbacks can then be used to send commands to the sensor (reset, calibrate, night mode, ...)
 * The callbacks can either have no parameter, or one int32_t parameter.
 */

namespace sensor_console {
  void defineCommand(const char *name, void (*function)(), const __FlashStringHelper *doc_fstring);
  void defineIntCommand(const char *name, void (*function)(int32_t), const __FlashStringHelper *doc_fstring);
  void defineStringCommand(const char *name, void (*function)(char*), const __FlashStringHelper *doc_fstring);

  void processSerialInput(const byte in_byte);

  void execute(const char *command_line);
}

#endif
