#ifndef AMPEL_UTIL_H_INCLUDED
#define AMPEL_UTIL_H_INCLUDED
#include <Arduino.h>
#include "config.h"
#include "sensor_console.h"

#include <WiFiUdp.h> // required for NTP
#include "src/lib/NTPClient-master/NTPClient.h" // NTP

#if defined(ESP8266)
#  include <ESP8266WiFi.h> // required to get MAC address
#  define esp_get_max_free_block_size() ESP.getMaxFreeBlockSize()
#  define esp_get_heap_fragmentation() ESP.getHeapFragmentation()
#elif defined(ESP32)
#  include <WiFi.h> // required to get MAC address
#  define esp_get_max_free_block_size() ESP.getMaxAllocHeap() //largest block of heap that can be allocated.
#  define esp_get_heap_fragmentation() "?" // apparently not available for ESP32
#endif

namespace ntp {
  void initialize();
  void update();
  void getLocalTime(char *timestamp);
}

namespace util {
  template<typename Tpa, typename Tpb>
  inline auto min(const Tpa &a, const Tpb &b) -> decltype(a < b ? a : b) {
    return b < a ? b : a;
  }

  template<typename Tpa, typename Tpb>
  inline auto max(const Tpa &a, const Tpb &b) -> decltype(b > a ? b : a) {
    return b > a ? b : a;
  }
}
class Ampel {
private:
  static void showFreeSpace();
public:
  const char *version = "v0.2.3-DEV"; // Update manually after significant changes.
  const char *board;
  const char *sensorId;
  const char *macAddress;
  uint32_t max_loop_duration;
  Ampel();
};

extern Ampel ampel;

//NOTE: Only use seconds() for duration comparison, not timestamps comparison. Otherwise, problems happen when millis roll over.
#define seconds() (millis() / 1000UL)

#endif
