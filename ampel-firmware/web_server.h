#ifndef WEB_SERVER_H_
#define WEB_SERVER_H_

#if defined(ESP8266)
#  include <ESP8266WebServer.h>
#elif defined(ESP32)
#  include <WebServer.h>
#endif

#include "config.h"
#include "util.h"
#include "wifi_util.h"
#include "co2_sensor.h"
#include "sensor_console.h"
#ifdef AMPEL_CSV
#  include "csv_writer.h"
#endif
#ifdef AMPEL_MQTT
#  include "mqtt.h"
#endif
#ifdef AMPEL_LORAWAN
#  include "lorawan.h"
#endif

namespace web_server {
  void initialize();
  void update();
}
#endif
