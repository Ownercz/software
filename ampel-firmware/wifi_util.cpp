#include "wifi_util.h"

namespace config {
  // WiFi config. See 'config.h' if you want to modify those values.
  const char *wifi_ssid = WIFI_SSID;
  const char *wifi_password = WIFI_PASSWORD;

#ifdef WIFI_TIMEOUT
  const uint8_t wifi_timeout = WIFI_TIMEOUT; // [s] Will try to connect during wifi_timeout seconds before failing.
#else
  const uint8_t wifi_timeout = 60;  // [s] Will try to connect during wifi_timeout seconds before failing.
#endif
}

namespace wifi {
  char local_ip[16]; // "255.255.255.255\0"

  void scanNetworks() {
    Serial.println();
    Serial.println(F("WiFi - Scanning..."));
    bool async = false;
    bool showHidden = true;
    int n = WiFi.scanNetworks(async, showHidden);
    for (int i = 0; i < n; ++i) {
      Serial.print(F("  * '"));
      Serial.print(WiFi.SSID(i));
      Serial.print(F("' ("));
      int16_t quality = 2 * (100 + WiFi.RSSI(i));
      Serial.print(util::min(util::max(quality, 0), 100));
      Serial.println(F(" %)"));
    }
    Serial.println(F("Done!"));
    Serial.println();
  }

  void showLocalIp() {
    Serial.print(F("WiFi - Local IP : "));
    Serial.println(wifi::local_ip);
    Serial.print(F("WiFi - SSID : "));
    Serial.println(WIFI_SSID);
  }

  // Initialize Wi-Fi
  void connect(const char *hostname) {

    sensor_console::defineCommand("wifi_scan", scanNetworks, F("(Scans available WiFi networks)"));
    sensor_console::defineCommand("local_ip", showLocalIp, F("(Displays local IP and current SSID)"));

    //NOTE: WiFi Multi could allow multiple SSID and passwords.
    WiFi.persistent(false); // Don't write user & password to Flash.
    WiFi.mode(WIFI_STA); // Set ESP to be a WiFi-client only
#if defined(ESP8266)
    WiFi.hostname(hostname);
#elif defined(ESP32)
    WiFi.setHostname(hostname);
#endif

    Serial.print(F("WiFi - Connecting to "));
    Serial.println(config::wifi_ssid);
    WiFi.begin(config::wifi_ssid, config::wifi_password);

    // Wait for connection, at most wifi_timeout seconds
    for (int i = 0; i <= config::wifi_timeout && (WiFi.status() != WL_CONNECTED); i++) {
      led_effects::showRainbowWheel();
      Serial.print(".");
    }
    if (WiFi.status() == WL_CONNECTED) {
      led_effects::showKITTWheel(color::green);
      Serial.println();
      Serial.print(F("WiFi - Connected! IP address: "));
      IPAddress address = WiFi.localIP();
      snprintf(local_ip, sizeof(local_ip), "%d.%d.%d.%d", address[0], address[1], address[2], address[3]);
      Serial.println(local_ip);
    } else {
      //TODO: Allow sensor to work as an Access Point, in order to define SSID & password?
      led_effects::showKITTWheel(color::red);
      Serial.println(F("Connection to WiFi failed"));
    }
  }
}
