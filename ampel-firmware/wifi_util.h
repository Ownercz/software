#ifndef WIFI_UTIL_H_INCLUDED
#define WIFI_UTIL_H_INCLUDED

#include "config.h"
#include "util.h"
#include "led_effects.h"

namespace wifi {
  extern char local_ip[];
  void connect(const char *hostname);
}

#endif
